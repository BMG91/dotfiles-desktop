#!/bin/bash

pacman -Syu wget

wget https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz
tar -xvf yay.tar.gz
cd yay/
makepkg -si

cd ..

git clone  https://gitlab.com/BMG91/dotfiles-desktop.git
cd dotfiles-desktop/.installer/
sudo pacman -S --needed - < packages.txt

cd ..

cp -r .config/ .local/ .mednafen/ .bashrc ~/

yay -S brave-bin dxvk-bin polybar protontricks stacer



neofetch

### EXPORT
export TERM="xterm-256color"              # getting proper colors
export TERMINAL="alacritty"
export BROWSER="brave"
#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '


##############################################################
##--------------------[-GENERAL-CONFIG-]--------------------##
##############################################################

#-[window-config]-#
for_window [class="^.*"] border pixel 0
gaps inner 7
gaps outer 7
# smart_gaps on

#-[enable-floating-on-certain-window-types]-#
for_window [window_role="pop-up"] floating enable
for_window [window_role="task_dialog"] floating enable
for_window [window_role="bubble"] floating enable
for_window [window_role="Preferences"] floating enable
for_window [window_type="dialog"] floating enable
for_window [window_type="menu"] floating enable

#-[enable-floating-on-apps]-#
for_window [class="Alacritty"] floating enable
for_window [class="Thunar"] floating enable
for_window [class="Pcmanfm"] floating enable
for_window [class="qBittorrent"] floating enable
# for_window [class="Gimp"] floating enable

#-[alt-key-instead-of-windows-key]-#
# set $mod mod1 
#-[set-$mod-(windows-key)]-#
set $mod Mod4

#-[set-$TERMINAL]-#
set $TERMINAL exec alacritty

#-[set-fonts]-#
# font pango:monospace 8
font pango:DejaVu Sans Mono 8

##############################################################
##---------------------[-KEY-BINDINGS-]---------------------##
##############################################################

#-[using-amixer-to-adjust-volume]-#
bindsym XF86AudioRaiseVolume exec "amixer sset Master 2%+ unmute"
bindsym XF86AudioLowerVolume exec "amixer sset Master 2%- unmute"
bindsym XF86AudioMute exec --no-startup-id amixer -q set Master toggle

#-[start-calculator-on-calculator-key]-#
bindsym XF86Calculator exec --no-startup-id qalculate-gtk

#-[audio-media-key-]-#
bindsym XF86Tools exec --no-startup-id deadbeef

#-[apps-shortcut-keys]-#
bindsym $mod+Shift+Return exec --no-startup-id pcmanfm
bindsym $mod+b exec --no-startup-id brave
bindsym $mod+g exec --no-startup-id geany
bindsym $mod+h exec --no-startup-id $TERMINAL -e htop
bindsym $mod+s exec --no-startup-id steam-native
bindsym $mod+l exec --no-startup-id lutris

#-[rofi-launch-key-and-flags-options]-#
#bindsym $mod+r exec rofi -show drun -show-icons -theme solarized
bindsym $mod+r exec "rofi -combi-modi window,drun,ssh -theme solarized -show combi -icon-theme 'Papirus-Dark' -show-icons"


#-[shortcut-key-to-connect-to-the-internet]-#
#bindsym $mod+F1 	--no-startup-id exec $TERMINAL -e nmtui

#-[use-mouse-$mod-to-drag-floating-windows-to-their-wanted-position]-#
floating_modifier $mod

#-[start-a-terminal]-#
bindsym $mod+Return exec --no-startup-id $TERMINAL

#-[kill-focused-window]-#
bindsym $mod+ctrl+c kill

#-[kill-window-on-middle-button-click]-#
#bindsym --whole-window $mod+button2 kill

#-[restore-screen-resolution]-#
bindsym $mod+mod1+r exec --no-startup-id $TERMINAL -e xrandr --output DP-4 --mode 1920x1080

#-[start-dmenu-(a-program-launcher)]-#
# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
# bindsym $mod+d exec --no-startup-id i3-dmenu-desktop
bindsym $mod+d exec --no-startup-id dmenu_run

#-[reload-the-configuration-file]-#
bindsym $mod+ctrl+F11 reload

#-[restart-i3-inplace-(preserves-your-layout/session,-can-be-used-to-upgrade-i3)]-#
bindsym $mod+ctrl+F12 restart

#-[exit-i3-(logs-you-out-of-your-X-session)]-#
#bindsym $mod+Shift+q exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"
bindsym $mod+ctrl+Escape  exec --no-startup-id ~/.config/i3/bin/i3_logout/i3_logout

##############################################################
##------------[-WORKSPACE-CONFIG-&-KEY-BINDINGS]------------##
##############################################################

#-[change-focus]-#
bindsym $mod+ctrl+h focus left
bindsym $mod+ctrl+j focus down
bindsym $mod+ctrl+k focus up
bindsym $mod+ctrl+l focus right

#-[alt,-you-can-use-the-cursor-keys]-#
# bindsym $mod+Left focus left
# bindsym $mod+Down focus down
# bindsym $mod+Up focus up
# bindsym $mod+Right focus right

#-[move-focused-window]-#
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

#-[alternatively,-you-can-use-the-cursor-keys]-#
# bindsym $mod+Shift+Left move left
# bindsym $mod+Shift+Down move down
# bindsym $mod+Shift+Up move up
# bindsym $mod+Shift+Right move right

#-[split-in-horizontal-orientation]-#
bindsym $mod+Shift+b split h

#-[split-in-vertical-orientation]-#
bindsym $mod+Shift+v split v

#-[enter-fullscreen-mode-for-the-focused-container]-#
bindsym $mod+ctrl+f fullscreen toggle

#-[change-container-layout-(stacked,-tabbed,-toggle-split)]-#
bindsym $mod+ctrl+s layout stacking
bindsym $mod+ctrl+t layout tabbed
bindsym $mod+ctrl+e layout toggle split

#-[toggle-tiling-/-floating]-#
bindsym $mod+Shift+space floating toggle

#-[change-focus-between-tiling-/-floating-windows]-#
bindsym $mod+space focus mode_toggle

#-[focus-the-parent-container]-#
bindsym $mod+ctrl+a focus parent

#-[focus-the-child-container]-#
bindsym $mod+ctrl+d focus child

#-[define-names-for-default-workspaces-for-which-we-configure-key-bindings-later-on]-#
#-[we-use-variables-to-avoid-repeating-the-names-in-multiple-places]-#
#-[needs-awesome-fonts4-from-the-aur]-#
set $ws1 "1.  Web"
set $ws2 "2.  Terminal"
set $ws3 "3.  Docs"
set $ws4 "4.  Music"
set $ws5 "5.  Steam"
set $ws6 "6.  VM's"
set $ws7 "7.  Videos"
set $ws8 "8.  Pics"
set $ws9 "9.  Other"


#-[apps-on-designated-workspaces]-#
assign [class="Brave"] $ws1
#	assign [class="Alacritty"] $ws2
assign [class="Geany"] $ws3
assign [class="libreoffice"] $ws3
assign [class="Deadbeef"] $ws4
assign [class="Steam"] $ws5
assign [class="VirtualBox Manager"] $ws6
assign [class="vlc"] $ws7
assign [class="Gimp"] $ws8

#-[switch-to-workspace]-#
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9


#-[move-focused-container-to-workspace]-#
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9

#-[resize-window-(you-can-also-use-the-mouse-for-that)]-#
bindsym $mod+Shift+r mode "resize"
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"        
}

##############################################################
##--------------------[-i3status-rust-]---------------------##
##############################################################
bar {
	i3bar_command i3bar -t
    font pango:Font Awesome 5 Free 9.7
    position top
    status_command /usr/bin/i3status-rs ~/.config/i3status-rust/config.toml
    colors {
        separator #666666
        background #00000068
        statusline #dddddd
        focused_workspace #0088CC #0088CC #ffffff
        active_workspace #333333 #333333 #ffffff
        inactive_workspace #333333 #333333 #888888
        urgent_workspace #2f343a #900000 #ffffff
    }
}

#-[hide/unhide-i3status-bar]-#
bindsym $mod+m bar mode toggle

#############################################################
##-----------------------[-STARTUP-]-----------------------##
#############################################################

#-[xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the]-#
#-[screen before suspend. Use loginctl lock-session to lock your screen-]
exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock --nofork

#-[wallpaper]-#
exec --no-startup-id nitrogen --restore
# exec --no-startup-id feh --bg-scale --no-fehbg ~/.config/wallpaper/wall.png

#-[network-manager]-#
# exec_always --no-startup-id nm-applet

#-[compositer]-#
exec --no-startup-id picom -b

# Num Lock
# exec_always --no-startup-id  bash -c "if xset q | grep -q 'Num Lock: *off'; then xdotool key Num_Lock; fi"

#-[xgamma for Asus Laptop]-#
#exec --no-startup-id xgamma -gamma 0.67

#-[volumeicon-for-audio-control]-#
# exec --no-startup-id volumeicon

#-[polybar]-#
# exec_always --no-startup-id $HOME/.config/polybar/launch.sh
